jQuery.exists = function (selector) { 
    return ($(selector).length > 0);
};

(function($) {
    if($.exists('.selectpicker')){
        $('.selectpicker').selectpicker();
    }


	if($.exists('.cabinet-img')){
		$(".cabinet-img-hide").hide();

		$('.cabinet-img').click(function(){
	    	$('.cabinet-img-hide').slideToggle();
	    });
	}

    if($.exists('.isotope')){
        $('.isotope').isotope({
            itemSelector: '.isotope-item'
        });
    }


})(jQuery);